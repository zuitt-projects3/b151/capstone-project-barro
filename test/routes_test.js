const chai = require('chai');

const expect = chai.expect;

const http = require('chai-http');

chai.use(http);

describe('Currency Test Suite', () => {

	it('Test Currency post if it returns 400 if no name property', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: "sweet love potion",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})	
	})
	it('Test API CURRENCY endpoint is running', () => {
		chai.request('http://localhost:5001')
		.post('/curency')
		.end((err,res) => {
			expect(res).to.not.equal(undefined)
		})
	})
	it('Test Currency post if it returns 400 if name is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: 12,
			alias: "sweet love potion",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})	
	})
	it('Test Currency post if it returns 400 if name is an empty string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "",
			alias: "sweet love potion",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})	
	})
	it('Test Currency post if it returns 400 if no ex property', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: "sweet love potion"
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})	
	})
	it('Test Currency post if it returns 400 if ex property is not an object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: "sweet love potion",
			ex: "hehehehe"
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})	
	})
	it('Test Currency post if it returns 400 if ex property is an empty object', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: "sweet love potion",
			ex: {}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})	
	})
	it('Test Currency post if it returns 400 if no alias property', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
			done();
		})	
	})
	it('Test Currency post if it returns 400 if alias is not a string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: true,
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})	
	})
	it('Test Currency post if it returns 400 if alias is an empty string', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: "",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})	
	})
	it('Test Currency post if it returns 400 if there are duplicate alias', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: "usd",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(400)
		})	
	})
	it('Test Currency post if it returns 200', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "slp",
			alias: "sweet love potion",
			ex: {
					'peso': 20,
          			'won': 2000.25,
          			'yen': 150.43,
          			'yuan': 15.95
				}
		})
		.end((err,res) => {
			expect(res.status).to.equal(200)
		})	
	})
})