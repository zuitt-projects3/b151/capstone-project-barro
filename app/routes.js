const {exchangeRates} = require('../src/util.js');

module.exports = (app) => {

	app.post('/currency', (req,res) => {

		if(!req.body.hasOwnProperty('name')){

			return res.status(400).send({
				'error': 'Bad Request: missing required parameter NAME'
			})
		}
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be string'
			})
		}
		if(typeof req.body.name === ''){
			return res.status(400).send({
				'error' : 'Bad Request : Empty String'
			})
		}
		if(!req.body.hasOwnProperty('ex')){

			return res.status(400).send({
				'error': 'Bad Request: missing required parameter EX'
			})
		}
		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error' : 'Bad Request : EX has to be an object'
			})
		}
		if(typeof req.body.ex === null){
			return res.status(400).send({
				'error' : 'Bad Request : Empty Object'
			})
		}
		if(!req.body.hasOwnProperty('alias')){

			return res.status(400).send({
				'error': 'Bad Request: missing required parameter alias'
			})
		}
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS has to be string'
			})
		}
		if(typeof req.body.alias === ''){
			return res.status(400).send({
				'error' : 'Bad Request : Empty String'
			})
		}
		if(req.body.alias === exchangeRates.usd.alias ||req.body.alias === exchangeRates.yen.alias || req.body.alias === exchangeRates.peso.alias || req.body.alias === exchangeRates.yuan.alias || req.body.alias === exchangeRates.won.alias){
			return res.status(400).send({
				'error': 'Bad Request: Duplicate ALIAS'
			})
		} else {
			return res.status(200)
		}

	});


};